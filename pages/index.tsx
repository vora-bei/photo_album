import Head from 'next/head'
import React, { useEffect, useState, useCallback } from 'react';
import styles from '../styles/Home.module.css'
import { Auth } from 'aws-amplify'
import { withAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react'
import Amplify, { API } from 'aws-amplify'
import config from '../src/aws-exports'
import Gallery from "react-photo-gallery";
import Carousel, { Modal, ModalGateway } from "react-images";
import ky from 'ky/umd';
const s3Url = 'https://dm2m7r5aqjm0v.cloudfront.net/';
Amplify.configure({
  ...config,
  ssr: true,
  API: {
    endpoints: [
      {
        name: "Upload",
        endpoint: "https://hqlk4n1af9.execute-api.us-east-2.amazonaws.com/dev/",
      }
    ]
  }
});

export default withAuthenticator(function Home() {
  const [user, setUser] = useState(null);
  const [uploads, setUploads] = useState([]);
  const handleDragEnter = e => {
    e.preventDefault();
    e.stopPropagation();
  };
  const handleDragLeave = e => {
    e.preventDefault();
    e.stopPropagation();
  };
  const handleDragOver = e => {
    e.preventDefault();
    e.stopPropagation();
  };
  function addUpload(file) {
    if (file) {
      var reader = new FileReader();
      reader.onload = function (e) {
        const img = new Image();
        img.onload = () => {
          setUploads([...uploads, { fullSrc: e.target.result, src: e.target.result, width: img.width, height: img.height }]);
        }
        img.src = e.target.result as string;
      }
      reader.readAsDataURL(file); // convert to base64 string
    }
  }

  useEffect(() => {
    if (user) {
      API.get('Upload', 'uploads', {})
        .then((res) => setUploads(res
          .map(
            ({ thumbpImagePath: src, imagePath, height = 1, width = 1 }) =>
              ({ src: s3Url + src, fullSrc: s3Url + imagePath, width, height })))
        );
    }
  }, [user])

  const handleUpload = useCallback(async (files) => {
    if (!files.length) {
      return;
    }
    const file = files[0];
    addUpload(file);
    const presignedPost = await API.post('Upload', 'createPostUploadUrl', {
      body: { filename: file.name },
    });

    const formData = new FormData();
    //formData.append("Content-Type", file.type);
    Object.entries(presignedPost.fields).forEach(([k, v]) => {
      formData.append(k, v as string);
    });

    formData.append("file", file);

    await ky.post(presignedPost.url, {
      body: formData,
    });
  }, [user, uploads]);

  const onUpload = useCallback(
    async (e: React.ChangeEvent<HTMLInputElement>) => {
      await handleUpload(e.currentTarget.files);
    }, [user, uploads]);
  useEffect(() => {
    // Access the user session on the client
    Auth.currentAuthenticatedUser()
      .then(user => {
        console.log("User: ", user)
        setUser(user)
      })
      .catch(err => setUser(null))
  }, [])
  const handleDrop = async (e) => {
    e.preventDefault();
    e.stopPropagation();
    let files = [...e.dataTransfer.files];
    await handleUpload(files);
  };
  const [currentImage, setCurrentImage] = useState(0);
  const [viewerIsOpen, setViewerIsOpen] = useState(false);

  const openLightbox = useCallback((event, { photo, index }) => {
    setCurrentImage(index);
    setViewerIsOpen(true);
  }, []);

  const closeLightbox = () => {
    setCurrentImage(0);
    setViewerIsOpen(false);
  };
  return (
    <div className={styles.body}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <header className={styles.header}>
        {user && <h1 className={styles.title}>Welcome, to photo album!</h1>}
        <AmplifySignOut className={styles.sign_out} />
      </header>
      <div className={styles.content}>
        <main className={styles.main}>
          <label
            className={styles.drop_area}
            onDrop={e => handleDrop(e)}
            onDragOver={e => handleDragOver(e)}
            onDragEnter={e => handleDragEnter(e)}
            onDragLeave={e => handleDragLeave(e)}
          >
            <input
              type="file"
              id="actual-btn"
              hidden
              onChange={onUpload}
            />
            <span className={styles.file_upload}>Select image</span>
          </label>
          <Gallery
            photos={uploads}
            onClick={openLightbox}
          />
          <ModalGateway>
            {viewerIsOpen ? (
              <Modal onClose={closeLightbox}>
                <Carousel
                  currentIndex={currentImage}
                  views={uploads.map(x => ({
                    ...x,
                    src: x.fullSrc,
                    caption: x.title
                  }))}
                />
              </Modal>
            ) : null}
          </ModalGateway>
        </main>
      </div>
      <footer className={styles.footer}>

      </footer>
    </div>
  )
})
